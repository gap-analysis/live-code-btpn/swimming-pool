package com.example.swimmingPoolTicket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TicketDTO {
    private long ticketId;
    private String ticketName;
    private int price;
    private int quantity;
}
