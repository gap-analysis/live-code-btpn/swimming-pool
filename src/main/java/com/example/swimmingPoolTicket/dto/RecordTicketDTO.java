package com.example.swimmingPoolTicket.dto;

import com.example.swimmingPoolTicket.entity.Ticket;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Data
public class RecordTicketDTO {
    private Date date = new Date();
    private Ticket ticket;
    private int quantity;
}
