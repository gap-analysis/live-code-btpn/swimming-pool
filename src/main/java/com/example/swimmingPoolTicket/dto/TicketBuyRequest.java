package com.example.swimmingPoolTicket.dto;

import com.example.swimmingPoolTicket.entity.Ticket;
import lombok.Data;

@Data
public class TicketBuyRequest {
    private Ticket ticket;
    private int quantity;
}
