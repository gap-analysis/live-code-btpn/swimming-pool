package com.example.swimmingPoolTicket.repository;

import com.example.swimmingPoolTicket.entity.RecordTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecordTicketRepository extends JpaRepository<RecordTicket, Long> {
}
