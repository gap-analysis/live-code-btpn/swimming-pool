package com.example.swimmingPoolTicket.service;

import com.example.swimmingPoolTicket.dto.TicketBuyRequest;
import com.example.swimmingPoolTicket.dto.TicketDTO;
import com.example.swimmingPoolTicket.entity.RecordTicket;
import com.example.swimmingPoolTicket.entity.Ticket;
import com.example.swimmingPoolTicket.repository.RecordTicketRepository;
import com.example.swimmingPoolTicket.repository.TicketRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class TicketService {

    @Autowired
    TicketRepository ticketRepository;
    @Autowired
    RecordTicketRepository recordTicketRepository;

    public ResponseEntity<?> createTicket(TicketDTO request) {
        try {
            Ticket newTicket = new Ticket();
            newTicket.setTicketName(request.getTicketName());
            newTicket.setPrice(request.getPrice());

            ticketRepository.save(newTicket);

            return ResponseEntity.ok(newTicket);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Gagal Create new Ticket!");
        }
    }

    public ResponseEntity<?> editTicket(TicketDTO request) {
        try {
            Ticket ticket = ticketRepository.findById(request.getTicketId()).orElse(null);

            if (ticket != null) {
                ticket.setTicketName(request.getTicketName());
                ticket.setPrice(request.getPrice());
                ticket.setQuantity(request.getQuantity());
                ticketRepository.save(ticket);
                return ResponseEntity.ok(ticket);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Ticket not found!");
            }

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Failed add quantity Ticket!");
        }
    }

    public ResponseEntity<?> buyTicket(TicketBuyRequest request) {
        log.info(request.toString());
        try {
            Ticket ticket = ticketRepository.findById(request.getTicket().getTicketId()).orElse(null);
            if (ticket != null) {
                if (ticket.getQuantity() < request.getQuantity()) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Quantity not enough!");
                } else {
                    ticket.setQuantity(ticket.getQuantity() - request.getQuantity());
                }
                ticketRepository.save(ticket);

                RecordTicket recordTicket = new RecordTicket();
                recordTicket.setTicket(ticket);
                recordTicket.setQuantity(request.getQuantity());

                recordTicketRepository.save(recordTicket);

                return ResponseEntity.ok("Sukses beli tiket!");
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Ticket not found!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Failed buy Ticket!");
        }
    }

    public ResponseEntity<?> getTicket(long id) {
        try {
            Ticket ticket = ticketRepository.findById(id).orElse(null);
            if (ticket != null) {
                return ResponseEntity.ok(ticket);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Ticket not found!");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Failed get Ticket!");
        }
    }

    public ResponseEntity<?> getAllTicket() {
        try {
            List<Ticket> listTicket = ticketRepository.findAll();
            if (listTicket != null) {
                return ResponseEntity.ok(listTicket);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Ticket not found!");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Failed get ALl Ticket!");
        }
    }

    public ResponseEntity<?> deleteTicket(long id) {
        try {
            Ticket ticket = ticketRepository.findById(id).orElse(null);
            if (ticket != null) {
                ticketRepository.delete(ticket);

                return ResponseEntity.ok("Sukses delete Ticket");
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Ticket not found!");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Failed delete Ticket!");
        }

    }
}
