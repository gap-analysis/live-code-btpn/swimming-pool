package com.example.swimmingPoolTicket.service;

import com.example.swimmingPoolTicket.entity.RecordTicket;
import com.example.swimmingPoolTicket.repository.RecordTicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordService {

    @Autowired
    RecordTicketRepository recordTicketRepository;

    public ResponseEntity<?> getAllRecord (){
        List<RecordTicket> listRecord = recordTicketRepository.findAll();

        return ResponseEntity.ok(listRecord);
    }
}
