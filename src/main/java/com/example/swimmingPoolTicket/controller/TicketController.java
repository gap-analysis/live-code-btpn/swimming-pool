package com.example.swimmingPoolTicket.controller;

import com.example.swimmingPoolTicket.dto.TicketBuyRequest;
import com.example.swimmingPoolTicket.dto.TicketDTO;
import com.example.swimmingPoolTicket.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/swimming-pool")
public class TicketController {

    @Autowired
    TicketService ticketService;

    @PostMapping("/create")
    @ResponseBody
    public ResponseEntity<?> createTicket(@RequestBody TicketDTO request){
        return ticketService.createTicket(request);
    }

    @PostMapping("/edit")
    @ResponseBody
    public ResponseEntity<?> editTicket (@RequestBody TicketDTO request){
        return ticketService.editTicket(request);
    }

    @PostMapping("/buy-ticket")
    @ResponseBody
    public ResponseEntity<?> buyTicket (@RequestBody TicketBuyRequest request){
        return ticketService.buyTicket(request);
    }

    @GetMapping("/get/{id}")
    @ResponseBody
    public ResponseEntity<?> getTicket (@PathVariable long id){
        return ticketService.getTicket(id);
    }

    @GetMapping("/getAll")
    @ResponseBody
    public ResponseEntity<?> getAllTicket (){
        return ticketService.getAllTicket();
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteTicket (@PathVariable long id){
        return ticketService.deleteTicket(id);
    }
}
