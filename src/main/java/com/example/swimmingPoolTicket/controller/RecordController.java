package com.example.swimmingPoolTicket.controller;

import com.example.swimmingPoolTicket.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/swimming-pool/record")
public class RecordController {

    @Autowired
    RecordService recordService;

    @GetMapping("/getAll")
    @ResponseBody
    public ResponseEntity<?> getAll (){
        return recordService.getAllRecord();
    }

}
