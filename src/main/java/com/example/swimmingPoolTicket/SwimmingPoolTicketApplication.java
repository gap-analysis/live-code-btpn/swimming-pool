package com.example.swimmingPoolTicket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwimmingPoolTicketApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwimmingPoolTicketApplication.class, args);
	}

}
