package com.example.swimmingPoolTicket.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "swimming_pool")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ticketId;

    @Column(name = "ticket_name")
    private String ticketName;

    @Column(name = "price")
    private int price;

    @Column(name = "quantity")
    private int quantity;
}
